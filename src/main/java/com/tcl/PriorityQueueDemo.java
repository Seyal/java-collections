package com.tcl;

import com.tcl.domain.Student;
import com.tcl.domain.StudentComparator;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueDemo {

    private Queue<Student> students = new PriorityQueue<>(new StudentComparator());

    public static void main(String[] args) {
        PriorityQueueDemo demo = new PriorityQueueDemo();

        demo.enrollStudent(new Student(500, "Kev"));
        demo.enrollStudent(new Student(1000, "Seth"));
        demo.enrollStudent(new Student(150, "Sue"));
        demo.enrollStudent(new Student(600, "Kacey"));
        demo.enrollStudent(new Student(650, "Kim"));

        assert demo.getNextStudent().getRollNo() == 150;
        assert demo.getNextStudent().getRollNo() == 500;
        assert demo.getNextStudent().getRollNo() == 600;
        assert demo.getNextStudent().getRollNo() == 650;
        assert demo.getNextStudent().getRollNo() == 1000;
        assert demo.getNextStudent() == null;

    }

    private void enrollStudent(Student s) {
        this.students.add(s);
    }

    //Should return Students by the order imposed by the StudentComparator that uses student roll number
    private Student getNextStudent() {
        return students.poll();
    }

}
